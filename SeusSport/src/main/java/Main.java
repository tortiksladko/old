import java.sql.*;
import java.util.Random;
import java.util.UUID;

public class Main {


    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement stmt = null;

        String url = "jdbc:sqlserver://10.5.0.52:1433;database=PABKSEUS;";
        String username = "sa";
        String password = "`123qwer";

        System.out.println("Connecting database...");
        DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        try {
            conn = DriverManager.getConnection(url, username, password);

            System.out.println("Database connected!");

            UUID uuid = UUID.randomUUID();
            Random rand = new Random();
            int randomNum = rand.nextInt((100000 - 10) + 1) + 10;


            String sql = "INSERT INTO PLAYER_ACCOUNTS (ACC_UID ,CLB_ID,PERSONAL_INFO_ID,OPEN_DATE_TIME,OPEN_DATE_TIME_TZ,CLOSE_DATE_TIME,CLOSE_DATE_TIME_TZ,RECEIPT_NUMBER,partsNumbers,PARI_CLASS_ID,NEED_PRINT,NEED_PRINT_PAYOUT,DRAWING_END,DRAWING_END_TZ,JACKBET_ID,JACKBET_ODDS,JACKBET_RESULT,JACKBET_SHIFT_ID,JACKBET_RES_ID,FREEBET_ID,FREEBET_INITIAL,FREEBET_CURRENT,FREEBET_ACTIVE,PAYIN,PAYOUT)" +
                    "VALUES ('"+uuid+"',2, "+randomNum+",'2019-02-04 11:54:41.660','180','2019-02-06 11:16:06.124','180'," +
                    "000800032833,0.0,4,1,1,null,null,null,null,null,null,null,null,null,null,null,0.0,0.0);";


            stmt = conn.createStatement();

            stmt.executeUpdate(sql);

        /*    PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
*/


            System.out.println(sql);

        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    conn.close();
            }catch(SQLException se){
            }// do nothing
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }//end main
}//end JDBCExample